﻿using System;
using System.IO;
using System.Windows;
using Salaros.Configuration;
using Console = Launcher.Views.Console;

namespace Launcher.Classes.Config
{
    public static class Doorstop
    {
        #region Default Content
        public const string DefaultContent = @"# General options for Unity Doorstop
[General]

# Enable Doorstop?
enabled=true

# Path to the assembly to load and execute
# NOTE: The entrypoint must be of format `static void Doorstop.Entrypoint.Start()`
target_assembly=VTOLVR_ModLoader\VTPatcher.dll

# If true, Unity's output log is redirected to <current folder>\output_log.txt
redirect_output_log=false

# If enabled, DOORSTOP_DISABLE env var value is ignored
# USE THIS ONLY WHEN ASKED TO OR YOU KNOW WHAT THIS MEANS
ignore_disable_switch=false


# Options specific to running under Unity Mono runtime
[UnityMono]

# Overrides default Mono DLL search path
# Sometimes it is needed to instruct Mono to seek its assemblies from a different path
# (e.g. mscorlib is stripped in original game)
# This option causes Mono to seek mscorlib and core libraries from a different folder before Managed
# Original Managed folder is added as a secondary folder in the search path
dll_search_path_override=

# If true, Mono debugger server will be enabled
debug_enabled=false

# When debug_enabled is true, specifies the address to use for the debugger server
debug_address=127.0.0.1:10000

# If true and debug_enabled is true, Mono debugger server will suspend the game execution until a debugger is attached
debug_suspend=false

# Options sepcific to running under Il2Cpp runtime
[Il2Cpp]

# Path to coreclr.dll that contains the CoreCLR runtime
coreclr_path=

# Path to the directory containing the managed core libraries for CoreCLR (mscorlib, System, etc.)
corlib_dir=
";
        #endregion

        public const string DefaultFileName = "doorstop_config.ini";

        private static string FilePath
        {
            get
            {
                if (string.IsNullOrEmpty(_filePath))
                {
                    _filePath = Path.Combine(Program.VTOLFolder, DefaultFileName);
                }

                return _filePath;
            }
        }
        private static string _filePath = string.Empty;

        public static void Enable()
        {
            if (!FileExists())
            {
                CreateDefaultFile();
                return;
            }
            
            if (IsOldDoorstepConfig())
            {
                File.Delete(FilePath);
                CreateDefaultFile();
                return;
            }
            
            ToggleDoorstep(true);
        }

        public static void Disable()
        {
            if (!FileExists())
            {
                CreateDefaultFile();
            }

            if (IsOldDoorstepConfig())
            {
                File.Delete(FilePath);
                CreateDefaultFile();
            }
            
            ToggleDoorstep(false);
        }

        public static bool FileExists() => File.Exists(FilePath);

        public static void CreateDefaultFile()
        {
            Console.Log("Writing Default doorstep config");
            File.WriteAllText(FilePath, DefaultContent);
        }

        private static void ToggleDoorstep(bool value)
        {
            try
            {
                ConfigParser config = new(FilePath);
                config.SetValue("General", "enabled", value);
                config.Save();
            }
            catch (Exception e)
            {
                Console.Log($"Failed to edit {FilePath}\n{e}");
                MessageBox.Show($"Failed to edit {FilePath}. Please join the discord and contact Marsh", "Error", MessageBoxButton.OK);
            }
        }

        private static bool IsOldDoorstepConfig()
        {
            var fileText = File.ReadAllLines(FilePath);
            
            // Some empty file?
            if (fileText.Length < 1)
                return true;

            var secondLine = fileText[1];
            return secondLine.Equals("[UnityDoorstop]");
        }
    }
}