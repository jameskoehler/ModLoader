﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModLoader.Classes
{
    public static class ModdedMPLobbyKeys
    {
        /// <summary>
        /// The steam lobby key for getting the servers modloader version
        /// </summary>
        public const string ModLoaderVersion = "ml_version";
    }
}
