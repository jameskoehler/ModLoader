﻿using VTPatcher;

namespace Doorstop
{
    public class Entrypoint
    {
        public static void Start() => Injector.Start();
    }
}